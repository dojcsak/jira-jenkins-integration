/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao;

import com.marvelution.jira.plugins.jenkins.model.Build;

/**
 * Interface for {@link Build} related Data Access
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public interface BuildDao {

	/**
	 * Get the {@link Build} by its ID
	 *
	 * @param buildId the build id
	 * @return the {@link Build}
	 */
	Build get(int buildId);

	/**
	 * Get a single {@link Build} by its jobId reference and build number
	 *
	 * @param jobId       the jobId reference of the {@link Build} to get
	 * @param buildNumber the build number of the {@link Build}
	 * @return the {@link Build}, may be {@code null}
	 */
	Build get(int jobId, int buildNumber);

	/**
	 * Get all the {@link Build}s by there jobId reference
	 *
	 * @param jobId          the Id of the Job to get all the builds for
	 * @param includeDeleted flag whether to included deleted builds
	 * @return all the {@link Build}s
	 */
	Iterable<Build> getAllByJob(int jobId, boolean includeDeleted);

	/**
	 * Save the given {@link Build}
	 *
	 * @param build the {@link Build} to save
	 * @return the saved {@link Build}
	 */
	Build save(Build build);

	/**
	 * Remove a {@link Build}
	 *
	 * @param buildId the id of the {@link Build} to remove
	 * @return the id of the removed build, should match the {@code buildId} parameter
	 */
	int remove(int buildId);

	/**
	 * Remove all the {@link Build}s by the jobId given
	 *
	 * @param jobId the Id of the Job to delete all the builds for
	 * @return the build ids that where removed
	 */
	int[] removeAllByJob(int jobId);

	/**
	 * Mark the given {@link Build} as deleted on the remote site
	 *
	 * @param build the {@link Build} to mark as deleted
	 */
	void delete(Build build);

	/**
	 * Mark all the Builds with the jobId reference as deleted
	 *
	 * @param jobId the jobId reference to mark the builds of
	 */
	void deleteAllInJob(int jobId);

	/**
	 * Mark all the Builds up to the given number of the given job Id as deleted
	 *
	 * @param jobId       the jobId to mark all the builds from
	 * @param buildNumber the build number till which the build should be marked as deleted
	 */
	void deleteAllInJob(int jobId, int buildNumber);

}
