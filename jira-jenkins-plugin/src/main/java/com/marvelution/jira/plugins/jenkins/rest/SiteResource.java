/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.marvelution.jira.plugins.jenkins.model.RestData;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.model.SiteList;
import com.marvelution.jira.plugins.jenkins.model.SiteStatus;
import com.marvelution.jira.plugins.jenkins.rest.security.AdminRequired;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST resource for {@link Site}s
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@Path("site")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@AdminRequired
public class SiteResource {

	private final SiteService siteService;
	private final JobService jobService;
	private final CommunicatorFactory communicatorFactory;
	private final JenkinsPluginUtil pluginUtil;

	/**
	 * Constructor
	 *
	 * @param siteService         the {@link com.marvelution.jira.plugins.jenkins.services.SiteService} implementation
	 * @param jobService          the {@link com.marvelution.jira.plugins.jenkins.services.JobService} implementation
	 * @param communicatorFactory the {@link com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory} implementation
	 * @param pluginUtil          the {@link JenkinsPluginUtil} implementation
	 */
	public SiteResource(SiteService siteService, JobService jobService, CommunicatorFactory communicatorFactory,
	                    JenkinsPluginUtil pluginUtil) {
		this.siteService = siteService;
		this.jobService = jobService;
		this.communicatorFactory = communicatorFactory;
		this.pluginUtil = pluginUtil;
	}

	/**
	 * Get all the sites available
	 *
	 * @return collection of all the sites
	 */
	@GET
	public Response getAll(@QueryParam("includeJobs") @DefaultValue("false") boolean includeJobs) {
		List<Site> sites = siteService.getAll(includeJobs);
		return Response.ok(new SiteList(sites)).build();
	}

	/**
	 * Get a {@link Site} by its Id
	 *
	 * @param siteId the site id
	 * @return Ok Response in case there is a site with the given Id and a Not Found otherwise
	 */
	@GET
	@Path("{siteId}")
	public Response get(@PathParam("siteId") int siteId, @QueryParam("includeJobs") @DefaultValue("false") boolean
			includeJobs) {
		Site site = siteService.get(siteId);
		if (site == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("No Site with id: " + siteId).build();
		} else {
			if (includeJobs) {
				site.setJobs(jobService.getAllBySite(site));
			}
			return Response.ok(site).build();
		}
	}

	/**
	 * Sync the {@link Site} by its given Id
	 *
	 * @param siteId the id of the Site to sync
	 * @return {@link Response}
	 */
	@POST
	@Path("{siteId}/sync")
	public Response syncJobList(@PathParam("siteId") int siteId) {
		Site site = siteService.get(siteId);
		if (site != null) {
			jobService.syncJobList(site);
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Site with id: " + siteId).build();
		}
	}

	/**
	 * Enable or Disable the auto linking of the site
	 *
	 * @param siteId   the id of the {@link Site}
	 * @param restData the {@link RestData} payload send
	 * @return 200 OK if the site is found, 404 NOT FOUND is not and 406 NOT ACCEPTED if the site doesn't support
	 *         auto linking
	 */
	@POST
	@Path("{siteId}/autolink")
	public Response enableAutoLink(@PathParam("siteId") int siteId, RestData restData) {
		Site site = siteService.get(siteId);
		if (site != null) {
			siteService.enable(siteId, Boolean.parseBoolean(restData.getPayload()));
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Site with id: " + siteId).build();
		}
	}

	/**
	 * Check is the {@link Site} is online and accessible
	 *
	 * @param siteId the id of the {@link Site}
	 * @return {@link SiteStatus}
	 */
	@GET
	@Path("{siteId}/status")
	public Response siteStatus(@PathParam("siteId") int siteId) {
		SiteStatus status;
		Site site = siteService.get(siteId);
		if (site != null) {
			Communicator communicator = communicatorFactory.get(site);
			try {
				if (!communicator.isRemoteOnlineAndAccessible()) {
					status = SiteStatus.offline(site);
				} else if (!communicator.isJenkinsPluginInstalled()) {
					status = SiteStatus.onlineNoPlugin(site).setMessage(pluginUtil.getJenkinsPluginUrl().toASCIIString());
				} else {
					status = SiteStatus.online(site);
				}
			} catch (CredentialsRequiredException e) {
				status = SiteStatus.notAccessible(site).setMessage(e.getMessage());
			}
			return Response.ok(status).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Site with id: " + siteId).build();
		}
	}

}
