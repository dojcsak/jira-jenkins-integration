/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.model;

import com.google.common.base.Objects;

import javax.xml.bind.annotation.*;

/**
 * Test Results Model
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TestResults {

	@XmlAttribute
	private int id = 0;
	@XmlAttribute
	private int buildId = 0;
	@XmlElement
	private int failed = 0;
	@XmlElement
	private int skipped = 0;
	@XmlElement
	private int total = 0;

	TestResults() {
	}

	public TestResults(int id, int buildId, int failed, int skipped, int total) {
		this.id = id;
		this.buildId = buildId;
		this.failed = failed;
		this.skipped = skipped;
		this.total = total;
	}

	public TestResults(int failed, int skipped, int total) {
		this(0, 0, failed, skipped, total);
	}

	public int getId() {
		return id;
	}

	public int getBuildId() {
		return buildId;
	}

	public int getFailed() {
		return failed;
	}

	public int getSkipped() {
		return skipped;
	}

	public int getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("buildId", buildId).add("total", total).toString();
	}

}
