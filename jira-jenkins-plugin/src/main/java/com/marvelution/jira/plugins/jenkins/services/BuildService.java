/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services;

import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public interface BuildService {

	/**
	 * Get a single {@link Build} by its ID
	 *
	 * @param buildId the {@link Build} id
	 * @return the {@link Build}
	 */
	Build get(int buildId);

	/**
	 * Get a single {@link Build} by its {@link Job} and build number
	 *
	 * @param job         the {@link Job} of the {@link Build} to get
	 * @param buildNumber the build number of the {@link Build}
	 * @return the {@link Build}, may be {@code null}
	 */
	Build get(Job job, int buildNumber);

	/**
	 * Get all {@link Build}s of the given {@link Job}
	 *
	 * @param job the {@link Job} to get all the builds for
	 * @return all the {@link Build}s that relate to the given {@link Job}
	 */
	Iterable<Build> getByJob(Job job);

	/**
	 * Get all the {@link Build}s related to the given JIRA issue key
	 *
	 * @param issueKey the JIRA issue key to get all the related {@link Build}s for
	 * @return all the {@link Build}s that relate to the given JIRA issue key
	 */
	Iterable<Build> getByIssueKey(String issueKey);

	/**
	 * Get all the {@link Build}s related to the given JIRA project key
	 *
	 * @param projectKey the JIRA project key to get all the related {@link Build}s for
	 * @return all the {@link Build}s that relate to the given JIRA project key
	 */
	Iterable<Build> getByProjectKey(String projectKey);

	/**
	 * Get all the builds that match the {@link BuildIssueFilter} given
	 *
	 * @param maxResults the maximum number of builds te return
	 * @param filter the {@link BuildIssueFilter}
	 * @return all the {@link Build}s that match the given filter
	 */
	Iterable<Build> getLatestBuildsByFilter(int maxResults, BuildIssueFilter filter);

	/**
	 * Get all related issue keys for the given {@link Build}
	 *
	 * @param build the {@link Build}
	 * @return the collection of issue keys
	 */
	Iterable<String> getRelatedIssueKeys(Build build);

	/**
	 * Get the number of Issue Keys that are linked to the given {@link Build}
	 *
	 * @param build the {@link Build}
	 * @return the number of related issues (zero or higher)
	 */
	int getRelatedIssueKeyCount(Build build);

	/**
	 * Get all related project keys for the given {@link Build}
	 *
	 * @param build the {@link Build}
	 * @return the collection of project keys
	 */
	Iterable<String> getRelatedProjectKeys(Build build);

	/**
	 * Link the given {@link Build} and issue key
	 *
	 * @param build    the {@link Build} to link to issue key
	 * @param issueKey the issue key to link
	 * @return {@code true} in case of a successful link, {@code false} otherwise
	 */
	boolean link(Build build, String issueKey);

	/**
	 * Save the given {@link Build}
	 *
	 * @param build the {@link Build} to save
	 * @return the saved {@link Build}
	 */
	Build save(Build build);

	/**
	 * Remove the {@link Build} given
	 *
	 * @param build the {@link Build} to remove
	 */
	void remove(Build build);

	/**
	 * Remove all {@link Build}s related to the given {@link Job}
	 *
	 * @param job the {@link Job} of which to remove all the {@link Build}
	 */
	void removeAllInJob(Job job);

	/**
	 * Mark the given {@link Build} as deleted on the remote site
	 *
	 * @param build the {@link Build} to mark as deleted
	 */
	void delete(Build build);

	/**
	 * Mark all the Builds of the given {@link Job} as deleted
	 *
	 * @param job the {@link Job} to mark all the builds from
	 */
	void deleteAllInJob(Job job);

	/**
	 * Mark all the Builds up to the given number of the given {@link Job} as deleted
	 *
	 * @param job the {@link Job} to mark all the builds from
	 * @param buildNumber the build number till which the build should be marked as deleted
	 */
	void deleteAllInJob(Job job, int buildNumber);
}
