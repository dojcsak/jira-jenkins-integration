/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Site mappings
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@Preload
public interface SiteMapping extends Entity {

	String APPLINK_ID = "APPLINK_ID";
	String NAME = "NAME";
	String AUTO_LINK = "AUTO_LINK";
	String SUPPORTS_BACK_LINK = "SUPPORTS_BACK_LINK";

	String getApplinkId();

	void setApplinkId(String applinkId);

	String getName();

	void setName(String name);

	boolean isAutoLink();

	void setAutoLink(boolean autoLink);

	boolean isSupportsBackLink();

	void setSupportsBackLink(boolean supportsBackLink);

}
