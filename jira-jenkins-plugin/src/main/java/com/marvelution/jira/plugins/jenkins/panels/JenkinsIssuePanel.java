/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.panels;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.action.IssueActionComparator;
import com.atlassian.jira.plugin.issuetabpanel.*;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.model.Build;

import java.util.Collections;
import java.util.List;

/**
 * The {@link AbstractIssueTabPanel2} implementation to get all the {@link Build}s related to teh {@link Issue}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsIssuePanel extends AbstractIssueTabPanel2 {

	private final PermissionManager permissionManager;
	private final BuildPanelHelper buildPanelHelper;

	/**
	 * Constructor
	 *
	 * @param permissionManager the {@link PermissionManager} implementation
	 * @param buildPanelHelper the {@link BuildPanelHelper} implementation
	 */
	public JenkinsIssuePanel(PermissionManager permissionManager, BuildPanelHelper buildPanelHelper) {
		this.permissionManager = permissionManager;
		this.buildPanelHelper = buildPanelHelper;
	}

	@Override
	public GetActionsReply getActions(GetActionsRequest getActionsRequest) {
		List<IssueAction> buildActions = Lists.newArrayList();
		Iterable<? extends Build> builds = buildPanelHelper.getBuildsByRelation(getActionsRequest.issue());
		buildActions.addAll(buildPanelHelper.getBuildActions(builds, true));
		return GetActionsReply.create(buildActions);
	}

	@Override
	public ShowPanelReply showPanel(ShowPanelRequest showPanelRequest) {
		return ShowPanelReply.create(permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL,
				showPanelRequest.issue(), showPanelRequest.remoteUser()));
	}

}
