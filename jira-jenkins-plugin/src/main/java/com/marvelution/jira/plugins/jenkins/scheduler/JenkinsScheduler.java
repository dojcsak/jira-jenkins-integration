/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.scheduler;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import javax.annotation.PreDestroy;
import java.util.Date;
import java.util.Map;

/**
 * Jenkins Scheduler implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class JenkinsScheduler implements LifecycleAware, DisposableBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsScheduler.class);
	private static final String JOB_NAME = JenkinsScheduler.class.getName() + ":job";
	private static final String PROPERTY_KEY = "jenkins.connector.scheduler.interval";
	private static final long DEFAULT_INTERVAL = 3600000L;
	private final PluginScheduler pluginScheduler;
	private final SiteService siteService;
	private final CommunicatorFactory communicatorFactory;
	private final JobService jobService;

	/**
	 * Constructor
	 *
	 * @param pluginScheduler     the {@link com.atlassian.sal.api.scheduling.PluginScheduler} implementation
	 * @param siteService         the {@link com.marvelution.jira.plugins.jenkins.services.SiteService} implementation
	 * @param communicatorFactory the {@link CommunicatorFactory} implementation
	 * @param jobService          the {@link com.marvelution.jira.plugins.jenkins.services.JobService} implementation
	 */
	public JenkinsScheduler(PluginScheduler pluginScheduler, SiteService siteService,
	                        CommunicatorFactory communicatorFactory, JobService jobService) {
		this.pluginScheduler = pluginScheduler;
		this.siteService = siteService;
		this.communicatorFactory = communicatorFactory;
		this.jobService = jobService;
	}

	@Override
	public void onStart() {
		LOGGER.info("Starting the Jenkins Scheduler");
		String property = System.getProperty(PROPERTY_KEY, "" + DEFAULT_INTERVAL);
		long interval;
		try {
			interval = Long.parseLong(property);
		} catch (Exception e) {
			interval = DEFAULT_INTERVAL;
		}
		LOGGER.debug("Starting Jenkins Connector Scheduler Job. interval=" + interval);
		Map<String, Object> data = Maps.newHashMap();
		data.put(JenkinsSchedulerJob.SITE_SERVICE, siteService);
		data.put(JenkinsSchedulerJob.JOB_SERVICE, jobService);
		data.put(JenkinsSchedulerJob.COMMUNICATOR_FACTORY, communicatorFactory);
		pluginScheduler.scheduleJob(JOB_NAME, JenkinsSchedulerJob.class, data, new Date(), interval);
	}

	/**
	 * Cleanup just before the bean is set to be destroyed.
	 * This will unschedule the synchronization job scheduled on start
	 */
	@PreDestroy
	@Override
	public void destroy() throws Exception {
		LOGGER.info("Stopping the Jenkins Scheduler");
		pluginScheduler.unscheduleJob(JOB_NAME);
	}

}
