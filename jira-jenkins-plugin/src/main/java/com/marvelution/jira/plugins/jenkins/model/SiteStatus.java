/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.*;

/**
 * Site Status model
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@XmlRootElement(name = "site-status")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteStatus {

	@XmlElement
	private Status status;
	@XmlElement
	private String message;
	@XmlElement
	private Site site;

	/**
	 * Package JAX-B Constructor
	 */
	/* package */ SiteStatus() {
	}

	/**
	 * Private Constructor
	 *
	 * @param status the {@link Status}
	 */
	private SiteStatus(Status status, Site site) {
		this.status = status;
		this.site = site;
	}

	/**
	 * Getter for the {@link #status}
	 *
	 * @return the {@link Status}
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Getter for the {@link #site}
	 *
	 * @return the {@link #site}
	 */
	public Site getSite() {
		return site;
	}

	/**
	 * Getter for the {@link #message}
	 *
	 * @return the {@link #message}
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setter for the {@link #message}
	 *
	 * @param message the {@link #message}
	 * @return the {@link SiteStatus}
	 */
	public SiteStatus setMessage(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Create a new {@link SiteStatus} with an {@link SiteStatus.Status#ONLINE}
	 *
	 * @return the {@link SiteStatus}
	 */
	public static SiteStatus online(Site site) {
		return new SiteStatus(Status.ONLINE, site);
	}

	/**
	 * Create a new {@link SiteStatus} with an {@link SiteStatus.Status#ONLINE_NO_PLUGIN}
	 *
	 * @return the {@link SiteStatus}
	 */
	public static SiteStatus onlineNoPlugin(Site site) {
		return new SiteStatus(Status.ONLINE_NO_PLUGIN, site);
	}

	/**
	 * Create a new {@link SiteStatus} with an {@link SiteStatus.Status#OFFLINE}
	 *
	 * @return the {@link SiteStatus}
	 */
	public static SiteStatus offline(Site site) {
		return new SiteStatus(Status.OFFLINE, site);
	}

	/**
	 * Create a new {@link SiteStatus} with an {@link SiteStatus.Status#NOT_ACCESSIBLE}
	 *
	 * @return the {@link SiteStatus}
	 */
	public static SiteStatus notAccessible(Site site) {
		return new SiteStatus(Status.NOT_ACCESSIBLE, site);
	}

	/**
	 * The Status types
	 */
	@XmlEnum(String.class)
	public enum Status {
		ONLINE, ONLINE_NO_PLUGIN, OFFLINE, NOT_ACCESSIBLE;
	}

}
