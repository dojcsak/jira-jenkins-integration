/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import org.apache.commons.lang.StringUtils;

import java.util.Set;

/**
 * BuildIssueFilter SQL Where Claus Builder
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BuildIssueFilterQueryBuilder {

	private static final String AND = " AND ";
	private static final String IN = " in ";
	private static final String NOT_IN = " not" + IN;

	private final BuildIssueFilter filter;

	/**
	 * Constructor
	 *
	 * @param filter the {@link BuildIssueFilter}
	 */
	public BuildIssueFilterQueryBuilder(BuildIssueFilter filter) {
		this.filter = filter;
	}

	/**
	 * Build the SQL Where claus from the {@link BuildIssueFilter}
	 *
	 * @return the SQL where claus
	 */
	public String build() {
		StringBuilder projectClaus = new StringBuilder();
		StringBuilder issueClaus = new StringBuilder();
		if (filter != null) {
			if (filter.getInProjectKeys() != null && !filter.getInProjectKeys().isEmpty()) {
				projectClaus.append(IssueMapping.PORJECT_KEY).append(IN).append(joinToSqlIn(filter.getInProjectKeys()));
			}
			if (filter.getNotInProjectKeys() != null && !filter.getNotInProjectKeys().isEmpty()) {
				if (projectClaus.length() != 0) {
					projectClaus.append(AND);
				}
				projectClaus.append(IssueMapping.PORJECT_KEY).append(NOT_IN).append(joinToSqlIn(filter
						.getNotInProjectKeys()));
			}
			if (filter.getInIssueKeys() != null && !filter.getInIssueKeys().isEmpty()) {
				issueClaus.append(IssueMapping.ISSUE_KEY).append(IN).append(joinToSqlIn(filter.getInIssueKeys()));
			}
			if (filter.getNotInIssueKeys() != null && !filter.getNotInIssueKeys().isEmpty()) {
				if (issueClaus.length() != 0) {
					issueClaus.append(AND);
				}
				issueClaus.append(IssueMapping.ISSUE_KEY).append(NOT_IN).append(joinToSqlIn(filter
						.getNotInIssueKeys()));
			}
		}
		StringBuilder finalClaus = new StringBuilder();
		if (projectClaus.length() != 0) {
			finalClaus.append("(").append(projectClaus).append(")");
		}
		if (issueClaus.length() != 0) {
			if (finalClaus.length() != 0) {
				finalClaus.append(AND);
			}
			finalClaus.append("(").append(issueClaus).append(")");
		}
		if (finalClaus.length() == 0) {
			finalClaus.append("true");
		}
		return finalClaus.toString();
	}

	/**
	 * Join the given {@link Set} of keys to be used in a SQL IN statement
	 *
	 * @param keys the keys to join
	 * @return the SQL formatted join
	 */
	private String joinToSqlIn(Set<String> keys) {
		StringBuilder joined = new StringBuilder("(");
		for (String key : keys) {
			if (StringUtils.isNotBlank(key)) {
				if (joined.length() > 1) {
					joined.append(", ");
				}
				joined.append("'").append(key).append("'");
			}
		}
		joined.append(")");
		return joined.toString();
	}

}
