/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services;

import com.google.common.collect.Sets;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Set;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BuildIssueFilter {

	private Set<String> inProjectKeys;
	private Set<String> notInProjectKeys;
	private Set<String> inIssueKeys;
	private Set<String> notInIssueKeys;
	private Set<String> notInUsers;
	private Set<String> inUsers;

	public Set<String> getInProjectKeys() {
		if (inProjectKeys == null) {
			inProjectKeys = Sets.newHashSet();
		}
		return inProjectKeys;
	}

	public void setInProjectKeys(Set<String> inProjectKeys) {
		this.inProjectKeys = inProjectKeys;
	}

	public Set<String> getNotInProjectKeys() {
		if (notInProjectKeys == null) {
			notInProjectKeys = Sets.newHashSet();
		}
		return notInProjectKeys;
	}

	public void setNotInProjectKeys(Set<String> notInProjectKeys) {
		this.notInProjectKeys = notInProjectKeys;
	}

	public Set<String> getInIssueKeys() {
		if (inIssueKeys == null) {
			inIssueKeys = Sets.newHashSet();
		}
		return inIssueKeys;
	}

	public void setInIssueKeys(Set<String> inIssueKeys) {
		this.inIssueKeys = inIssueKeys;
	}

	public Set<String> getNotInIssueKeys() {
		if (notInIssueKeys == null) {
			notInIssueKeys = Sets.newHashSet();
		}
		return notInIssueKeys;
	}

	public void setNotInIssueKeys(Set<String> notInIssueKeys) {
		this.notInIssueKeys = notInIssueKeys;
	}

	public Set<String> getNotInUsers() {
		if (notInUsers == null) {
			notInUsers = Sets.newHashSet();
		}
		return notInUsers;
	}

	public void setNotInUsers(Set<String> notInUsers) {
		this.notInUsers = notInUsers;
	}

	public Set<String> getInUsers() {
		if (inUsers == null) {
			inUsers = Sets.newHashSet();
		}
		return inUsers;
	}

	public void setInUsers(Set<String> inUsers) {
		this.inUsers = inUsers;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
