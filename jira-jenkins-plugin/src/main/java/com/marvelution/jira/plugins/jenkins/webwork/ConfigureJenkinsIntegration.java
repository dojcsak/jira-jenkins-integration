/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.webwork;

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@WebSudoRequired
public class ConfigureJenkinsIntegration extends JiraWebActionSupport {

	private final JenkinsPluginUtil pluginUtil;
	private final SiteService siteService;
	private final BuildUtilsInfo buildUtilsInfo;

	/**
	 * Constructor
	 *
	 * @param pluginUtil the {@link JenkinsPluginUtil}
	 * @param siteService the {@link com.marvelution.jira.plugins.jenkins.services.SiteService} implementation
	 */
	public ConfigureJenkinsIntegration(JenkinsPluginUtil pluginUtil, SiteService siteService, BuildUtilsInfo buildUtilsInfo) {
		this.pluginUtil = pluginUtil;
		this.siteService = siteService;
		this.buildUtilsInfo = buildUtilsInfo;
	}

	@Override
	@RequiresXsrfCheck
	protected String doExecute() throws Exception {
		return INPUT;
	}

	/**
	 * Check if there are sites configured
	 *
	 * @return {@code true} if there are sites configured, {@code false} otherwise
	 */
	public boolean hasSites() {
		return !siteService.getAll(false).isEmpty();
	}

	/**
	 * Get the major version of the JIRA instance the plugin is installed on
	 *
	 * @return the major version number
	 */
	public int getJiraMajorVersion() {
		return buildUtilsInfo.getVersionNumbers()[0];
	}

	/**
	 * Load all the {@link Site}s and sort them according to there names
	 *
	 * @return the sorted Array of {@link Site}s available
	 */
	public Site[] loadSites() {
		List<Site> sites = Lists.newArrayList(siteService.getAll(true));
		Collections.sort(sites, new Comparator<Site>() {
			@Override
			public int compare(Site site, Site site2) {
				return site.getName().compareTo(site2.getName());
			}
		});
		return sites.toArray(new Site[sites.size()]);
	}

	/**
	 * Getter for the {@link JenkinsPluginUtil}
	 *
	 * @return the {@link JenkinsPluginUtil}
	 */
	public JenkinsPluginUtil getPluginUtil() {
		return pluginUtil;
	}

}
