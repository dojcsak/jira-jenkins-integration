/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao;

import com.marvelution.jira.plugins.jenkins.model.Job;

import java.util.List;

/**
 * Interface for {@link Job} related Data Access
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public interface JobDao {

	/**
	 * Get all the {@link Job} objects that are related to the given applicationId
	 *
	 * @param siteId the {@link int} to get all the {@link Job} objects for
	 * @param includeDeleted  flag whether to include deleted Jobs
	 * @return the {@link List} of {@link Job} objects
	 */
	List<Job> getAllBySiteId(int siteId, boolean includeDeleted);

	/**
	 * Get all the {@link Job} objects configured
	 *
	 * @param includeDeleted  flag whether to include deleted Jobs
	 * @return the {@link List} of {@link Job} objects
	 */
	List<Job> getAll(boolean includeDeleted);

	/**
	 * Save the given job
	 *
	 * @param job the {@link Job} to save
	 * @return the saved {@link Job}
	 */
	Job save(Job job);

	/**
	 * Remove the {@link Job} with the jobId given
	 *
	 * @param jobId the ID of the {@link Job} to delete
	 */
	void remove(int jobId);

	/**
	 * Delete all the {@link Job} objects from the given {@link int}
	 *
	 * @param siteId the {@link int} to delete all the {@link Job} objects from
	 */
	void removeAllBySiteId(int siteId);

	/**
	 * Mark the given {@link Job} as deleted
	 *
	 * @param job the {@link Job} to be marked as deleted
	 */
	void delete(Job job);

	/**
	 * Get the {@link Job} object with the given jobId
	 *
	 * @param jobId the ID of the {@link Job} to get
	 * @return the {@link Job}
	 */
	Job get(int jobId);


	/**
	 * Get a {@link Job} by its name
	 *
	 * @param name the name for the {@link Job} to get
	 * @return a {@link List} of {@link Job}s, may be {@code empty} but not {@code null}
	 */
	List<Job> get(String name);

}
