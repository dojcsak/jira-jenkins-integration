/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.dao.BuildDao;
import com.marvelution.jira.plugins.jenkins.dao.IssueDao;
import com.marvelution.jira.plugins.jenkins.dao.TestResultDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Default {@link com.marvelution.jira.plugins.jenkins.services.BuildService} implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultBuildService implements BuildService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBuildService.class);
	private final Function<Build, Build> enrichBuild = new Function<Build, Build>() {
		@Override
		public Build apply(@Nullable Build input) {
			if (input != null) {
				input.setTestResults(testResultDao.getForBuild(input.getId()));
			}
			return input;
		}
	};
	private final BuildDao buildDao;
	private final IssueDao issueDao;
	private final TestResultDao testResultDao;

	/**
	 * Constructor
	 *
	 * @param buildDao      the {@link com.marvelution.jira.plugins.jenkins.dao.BuildDao} implementation
	 * @param issueDao      the {@link com.marvelution.jira.plugins.jenkins.dao.IssueDao} implementation
	 * @param testResultDao the {@link com.marvelution.jira.plugins.jenkins.dao.TestResultDao} implementation
	 */
	public DefaultBuildService(BuildDao buildDao, IssueDao issueDao, TestResultDao testResultDao) {
		this.buildDao = buildDao;
		this.issueDao = issueDao;
		this.testResultDao = testResultDao;
	}

	@Override
	public Build get(int buildId) {
		return enrichBuild.apply(buildDao.get(buildId));
	}

	@Override
	public Build get(Job job, int buildNumber) {
		return enrichBuild.apply(buildDao.get(job.getId(), buildNumber));
	}

	@Override
	public Iterable<Build> getByJob(Job job) {
		return Iterables.transform(buildDao.getAllByJob(job.getId(), false), enrichBuild);
	}

	@Override
	public Iterable<Build> getByIssueKey(String issueKey) {
		return getBuilds(issueDao.getLinksByIssueKey(issueKey));
	}

	@Override
	public Iterable<Build> getByProjectKey(String projectKey) {
		return getBuilds(issueDao.getLinksByProjectKey(projectKey));
	}

	/**
	 * Get all the {@link Build}s by there given Ids
	 *
	 * @param buildIds the build ids to get
	 * @return the collection of {@link Build} objects, may be {@code empty} but never {@code null}
	 */
	private Iterable<Build> getBuilds(Iterable<Integer> buildIds) {
		List<Build> builds = Lists.newArrayList();
		for (Integer buildId : buildIds) {
			Build build = buildDao.get(buildId);
			if (build == null) {
				LOGGER.debug("Failed to get back build mapping with id: {}", buildId);
			} else if (!build.isDeleted()) {
				builds.add(enrichBuild.apply(build));
			}
		}
		return builds;
	}

	@Override
	public Iterable<Build> getLatestBuildsByFilter(int maxResults, BuildIssueFilter filter) {
		return getBuilds(issueDao.getLatestLinksByFilter(maxResults, filter));
	}

	@Override
	public Iterable<String> getRelatedIssueKeys(Build build) {
		return issueDao.getIssueKeysByBuild(build);
	}

	@Override
	public int getRelatedIssueKeyCount(Build build) {
		return issueDao.getIssueLinkCount(build);
	}

	@Override
	public Iterable<String> getRelatedProjectKeys(Build build) {
		return issueDao.getProjectKeysByBuild(build);
	}

	@Override
	public boolean link(Build build, String issueKey) {
		return issueDao.link(build, issueKey);
	}

	@Override
	public Build save(Build build) {
		Build saved = buildDao.save(build);
		saved.setTestResults(testResultDao.save(saved.getId(), build.getTestResults()));
		// Copy over the collections that are not saved in the Build Mapping
		saved.getCulprits().addAll(build.getCulprits());
		saved.getArtifacts().addAll(build.getArtifacts());
		saved.getChangeSet().addAll(build.getChangeSet());
		return saved;
	}

	@Override
	public void remove(Build build) {
		testResultDao.removeTestResults(new int[] { buildDao.remove(build.getId()) });
	}

	@Override
	public void removeAllInJob(Job job) {
		testResultDao.removeTestResults(buildDao.removeAllByJob(job.getId()));
	}

	@Override
	public void delete(Build build) {
		buildDao.delete(build);
	}

	@Override
	public void deleteAllInJob(Job job) {
		buildDao.deleteAllInJob(job.getId());
	}

	@Override
	public void deleteAllInJob(Job job, int buildNumber) {
		buildDao.deleteAllInJob(job.getId(), buildNumber);
	}

}
