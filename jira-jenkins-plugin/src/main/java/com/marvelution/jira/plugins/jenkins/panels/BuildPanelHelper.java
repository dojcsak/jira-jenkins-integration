/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.panels;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.action.IssueActionComparator;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.utils.VelocityUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * CI Build Panel Helper
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BuildPanelHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildPanelHelper.class);
	private static final int MAX_RESULTS = -1;
	private final ApplicationProperties applicationProperties;
	private final TemplateRenderer templateRenderer;
	private final SearchProvider searchProvider;
	private final JobService jobService;
	private final BuildService buildService;
	private final VelocityUtils velocityUtils;

	/**
	 * Constructor
	 *
	 * @param applicationProperties the {@link com.atlassian.jira.config.properties.ApplicationProperties}
	 * @param templateRenderer      the {@link com.atlassian.templaterenderer.TemplateRenderer} implementation
	 * @param searchProvider        the {@link SearchProvider}
	 * @param jobService            the {@link com.marvelution.jira.plugins.jenkins.services.JobService} implementation
	 * @param buildService          the {@link com.marvelution.jira.plugins.jenkins.services.BuildService} implementation
	 * @param velocityUtils         the {@link com.marvelution.jira.plugins.jenkins.utils.VelocityUtils}
	 */
	public BuildPanelHelper(ApplicationProperties applicationProperties, TemplateRenderer templateRenderer,
	                        SearchProvider searchProvider, JobService jobService, BuildService buildService,
	                        VelocityUtils velocityUtils) {
		this.applicationProperties = applicationProperties;
		this.templateRenderer = templateRenderer;
		this.searchProvider = searchProvider;
		this.jobService = jobService;
		this.buildService = buildService;
		this.velocityUtils = velocityUtils;
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Issue}
	 *
	 * @param issue the {@link Issue} to get all the builds for
	 * @return the collection of {@link Build}s
	 */
	@NotNull
	public Iterable<? extends Build> getBuildsByRelation(@NotNull Issue issue) {
		LOGGER.debug("Looking for builds related to issue [{}]", issue.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInIssueKeys().add(issue.getKey());
		return buildService.getLatestBuildsByFilter(MAX_RESULTS, filter);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Project}
	 *
	 * @param project the {@link Project} to get all the builds for
	 * @return the collection of {@link Build}s
	 */
	@NotNull
	public Iterable<? extends Build> getBuildsByRelation(@NotNull Project project) {
		LOGGER.debug("Looking for builds related to project [{}]", project.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInProjectKeys().add(project.getKey());
		return buildService.getLatestBuildsByFilter(MAX_RESULTS, filter);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link ProjectComponent}
	 *
	 * @param component the {@link ProjectComponent} to get all the builds for
	 * @param user      the current logged in {@link User}
	 * @return the collection of {@link Build}s
	 */
	@NotNull
	public Iterable<? extends Build> getBuildsByRelation(@NotNull ProjectComponent component, @NotNull User user) {
		LOGGER.debug("Looking for builds related to component [{}]", component.getName());
		return getBuildsByRelation(JqlQueryBuilder.newBuilder().where().component(component.getId()).endWhere(), user);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Version}
	 *
	 * @param version the {@link Version} to get all the builds for
	 * @param user    the current logged in {@link User}
	 * @return the collection of {@link Build}s
	 */
	@NotNull
	public Iterable<? extends Build> getBuildsByRelation(@NotNull Version version, @NotNull User user) {
		LOGGER.debug("Looking for builds related to version [{}]", version.getName());
		return getBuildsByRelation(JqlQueryBuilder.newBuilder().where().affectedVersion(version.getName()).or()
				.fixVersion(version.getId()).endWhere(), user);
	}

	/**
	 * Get all the builds related to issues that match the given {@link JqlQueryBuilder}
	 *
	 * @param jql  the {@link JqlQueryBuilder} match issues against
	 * @param user the current logged in {@link User}
	 * @return the collection of builds that are related to issues matching th JQL,
	 *         may be {@code empty} but never {@code null}
	 */
	@NotNull
	private Iterable<? extends Build> getBuildsByRelation(@NotNull JqlQueryBuilder jql, @NotNull User user) {
		try {
			SearchResults results = searchProvider.search(jql.buildQuery(), user, PagerFilter.getUnlimitedFilter());
			BuildIssueFilter filter = new BuildIssueFilter();
			Iterable<String> keys = Iterables.transform(results.getIssues(), new Function<Issue, String>() {
				@Override
				public String apply(Issue issue) {
					return issue.getKey();
				}
			});
			Iterables.addAll(filter.getInIssueKeys(), keys);
			return buildService.getLatestBuildsByFilter(MAX_RESULTS, filter);
		} catch (SearchException e) {
			return Lists.newArrayList();
		}
	}

	/**
	 * Get {@link BuildAction}s for all the {@link Build}s given
	 *
	 * @param builds the {@link Build}s to get actions for
	 * @param sorted flag to sort the {@link BuildAction}s in reverse order, if false the list is not sorted!
	 * @return the {@link BuildAction}s
	 */
	@NotNull
	public List<BuildAction> getBuildActions(@NotNull Iterable<? extends Build> builds, boolean sorted) {
		List<BuildAction> buildActions = Lists.newArrayList();
		for (Build build : builds) {
			String html = getBuildActionHtml(build);
			if (StringUtils.isNotBlank(html)) {
				buildActions.add(new BuildAction(html, build.getBuildDate()));
			}
		}
		if (buildActions.isEmpty()) {
			buildActions.add(BuildAction.NO_BUILDS_ACTION);
		}
		if (sorted) {
			Collections.sort(buildActions, IssueActionComparator.COMPARATOR);
			Collections.reverse(buildActions);
		}
		return buildActions;
	}

	/**
	 * Generate a HTML View for the {@link Build} given
	 *
	 * @param build the {@link Build} to generate the HTML view for
	 * @return the HTML string, may be {@code empty} but not {@code null}
	 */
	@NotNull
	public String getBuildActionHtml(@NotNull Build build) {
		Job job = jobService.get(build.getJobId());
		if (job == null) {
			return "";
		}
		Map<String, Object> context = Maps.newHashMap();
		context.put("baseUrl", applicationProperties.getString(APKeys.JIRA_BASEURL));
		context.put("velocityUtils", velocityUtils);
		context.put("build", build);
		context.put("buildUrl", jobService.getJobBuildUrl(job, build));
		context.put("job", job);
		context.put("jobUrl", jobService.getJobUrl(job));
		if (buildService.getRelatedIssueKeyCount(build) <= 5) {
			context.put("issueKeys", Lists.newArrayList(buildService.getRelatedIssueKeys(build)));
		} else {
			context.put("projectKeys", Lists.newArrayList(buildService.getRelatedProjectKeys(build)));
		}
		StringWriter writer = new StringWriter();
		try {
			templateRenderer.render("/templates/jenkins-build.vm", context, writer);
		} catch (IOException e) {
			LOGGER.warn("Failed to render html for build {} -> {}", build.getId(), e.getMessage());
		}
		return writer.toString();
	}

}
