/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Site;

import java.util.List;

/**
 * Service interface for the {@link Site} objects
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public interface SiteService {

	/**
	 * Getter for a {@link Site} by its Id
	 *
	 * @param siteId the Id to get
	 * @return the {@link Site}, may be {@code null}
	 */
	Site get(int siteId);

	/**
	 * Getter for all the {@link Site}s
	 *
	 * @param includeJobs flag whether to include the jobs or not
	 * @return collection of all the {@link Site}s
	 */
	List<Site> getAll(boolean includeJobs);

	/**
	 * Getter of a {@link Site} by the given {@link ApplicationLink}
	 *
	 * @param applicationLink the {@link ApplicationLink} to get the {@link Site} for
	 * @return the {@link Site}, may be {@code null}
	 */
	Site getByApplicationLink(ApplicationLink applicationLink);

	/**
	 * Enable/Disable the default enabled state for new jobs of a Site
	 *
	 * @param siteId   the ID of the {@link Site} to update the synchronization state of
	 * @param enabled the synchronization state ({@code true} to enable synchronization and {@code false} to disable it)
	 */
	void enable(int siteId, boolean enabled);

	/**
	 * Save the given {@link Site}
	 *
	 * @param site the {@link Site} to save
	 * @return the saved {@link Site}
	 */
	Site save(Site site);

	/**
	 * Remove the {@link Site} by its {@link ApplicationLink}
	 *
	 * @param applicationLink the {@link ApplicationLink} to be removed
	 */
	void removeByApplicationLink(ApplicationLink applicationLink);

	/**
	 * Update the {@link ApplicationLink} for the {@link Site} that has the oldApplicationId
	 *
	 * @param applicationLink  the new {@link ApplicationLink}
	 * @param oldApplicationId the old {@link ApplicationId}
	 */
	void updateApplicationLink(ApplicationLink applicationLink, ApplicationId oldApplicationId);

	/**
	 * Get the {@link ApplicationLink} for the given {@link Site}
	 *
	 * @param site the {@link Site} to get the {@link ApplicationLink} for
	 * @return the {@link ApplicationLink}
	 */
	ApplicationLink getApplicationLink(Site site);

	/**
	 * Get the {@link ApplicationLink} for the given {@link com.marvelution.jira.plugins.jenkins.model.Job}
	 *
	 * @param job the {@link com.marvelution.jira.plugins.jenkins.model.Job} to get the {@link ApplicationLink} for
	 * @return the {@link ApplicationLink}
	 */
	ApplicationLink getApplicationLink(Job job);

}
