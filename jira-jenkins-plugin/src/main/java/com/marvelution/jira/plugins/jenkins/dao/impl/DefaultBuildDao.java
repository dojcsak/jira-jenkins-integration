/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.ao.BuildMapping;
import com.marvelution.jira.plugins.jenkins.dao.BuildDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import net.java.ao.Query;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Default {@link BuildDao} implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultBuildDao implements BuildDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBuildDao.class);
	private static final int MAX_SIZE = 200;
	private final ActiveObjects ao;
	private final Function<BuildMapping, Build> buildMappingToBuildFunction = new Function<BuildMapping, Build>() {
		@Override
		public Build apply(@Nullable BuildMapping from) {
			if (from == null) {
				return null;
			}
			Build build = new Build(from.getID(), from.getJobId(), from.getBuildNumber());
			build.setCause(from.getCause());
			build.setDuration(from.getDuration());
			build.setTimestamp(from.getTimeStamp());
			build.setResult(from.getResult());
			build.setBuiltOn(from.getBuiltOn());
			build.setDeleted(from.isDeleted());
			return build;
		}
	};

	/**
	 * Constructor
	 *
	 * @param ao the {@link ActiveObjects} implementation
	 */
	public DefaultBuildDao(ActiveObjects ao) {
		this.ao = ao;
	}

	@Override
	public Build get(final int buildId) {
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				return ao.get(BuildMapping.class, buildId);
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public Build get(final int jobId, final int buildNumber) {
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				BuildMapping[] mappings = ao.find(BuildMapping.class, Query.select().where(BuildMapping.JOB_ID + " =  ? AND " +
						BuildMapping.BUILD_NUMBER + " = ?", jobId, buildNumber));
				if (mappings != null && mappings.length == 1) {
					return mappings[0];
				} else {
					return null;
				}
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public Iterable<Build> getAllByJob(final int jobId, final boolean includeDeleted) {
		List<BuildMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<BuildMapping>>() {
			@Override
			public List<BuildMapping> doInTransaction() {
				Query query = Query.select();
				if (includeDeleted) {
					query.where(BuildMapping.JOB_ID + " = ? AND " + BuildMapping.DELETED + " = ?", jobId, Boolean.FALSE);
				} else {
					query.where(BuildMapping.JOB_ID + " = ?", jobId);
				}
				query.order(BuildMapping.TIME_STAMP);
				return Arrays.asList(ao.find(BuildMapping.class, query));
			}
		});
		return Lists.transform(mappings, buildMappingToBuildFunction);
	}

	@Override
	public Build save(final Build build) {
		LOGGER.debug("Saving {}", build.toString());
		BuildMapping mapping = ao.executeInTransaction(new TransactionCallback<BuildMapping>() {
			@Override
			public BuildMapping doInTransaction() {
				BuildMapping mapping;
				if (build.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(BuildMapping.JOB_ID, build.getJobId());
					map.put(BuildMapping.BUILD_NUMBER, build.getNumber());
					map.put(BuildMapping.CAUSE, StringUtils.substring(build.getCause(), 0, MAX_SIZE));
					map.put(BuildMapping.RESULT, build.getResult());
					map.put(BuildMapping.DURATION, build.getDuration());
					map.put(BuildMapping.TIME_STAMP, build.getTimestamp());
					map.put(BuildMapping.BUILT_ON, build.getBuiltOn());
					map.put(BuildMapping.DELETED, build.isDeleted());
					mapping = ao.create(BuildMapping.class, map);
					mapping = ao.find(BuildMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(BuildMapping.class, build.getId());
					mapping.setJobId(build.getJobId());
					mapping.setBuildNumber(build.getNumber());
					mapping.setCause(StringUtils.substring(build.getCause(), 0, MAX_SIZE));
					mapping.setResult(build.getResult());
					mapping.setDuration(build.getDuration());
					mapping.setTimeStamp(build.getTimestamp());
					mapping.setBuiltOn(build.getBuiltOn());
					mapping.setDeleted(build.isDeleted());
					mapping.save();
				}
				return mapping;
			}
		});
		return buildMappingToBuildFunction.apply(mapping);
	}

	@Override
	public int remove(int buildId) {
		ao.delete(ao.get(BuildMapping.class, buildId));
		return buildId;
	}

	@Override
	public int[] removeAllByJob(final int jobId) {
		return ao.executeInTransaction(new TransactionCallback<int[]>() {
			@Override
			public int[] doInTransaction() {
				BuildMapping[] mappings = ao.find(BuildMapping.class, Query.select().where(BuildMapping.JOB_ID + "= ?", jobId));
				if (mappings == null) {
					return new int[0];
				} else {
					int[] buildIds = new int[mappings.length];
					for (int i = 0; i < mappings.length; i++) {
						buildIds[i] = mappings[i].getID();
					}
					ao.delete(mappings);
					return buildIds;
				}
			}
		});
	}

	@Override
	public void delete(Build build) {
		build.setDeleted(true);
		save(build);
	}

	@Override
	public void deleteAllInJob(int jobId) {
		deleteAllInJob(jobId, -1);
	}

	@Override
	public void deleteAllInJob(final int jobId, final int buildNumber) {
		ao.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				Query query = Query.select();
				if (buildNumber > 0) {
					query.where(BuildMapping.JOB_ID + " = ? AND " + BuildMapping.BUILD_NUMBER + " < ?", jobId, buildNumber);
				} else {
					query.where(BuildMapping.JOB_ID + " = ?", jobId);
				}
				BuildMapping[] mappings = ao.find(BuildMapping.class, query);
				for (BuildMapping mapping : mappings) {
					mapping.setDeleted(true);
					mapping.save();
				}
				return null;
			}
		});
	}

}
