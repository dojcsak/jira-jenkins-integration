/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.listeners;

import com.atlassian.applinks.api.event.ApplicationLinkEvent;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonApplicationType;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Base ApplicationLink Event listener
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class AbstractJenkinsApplicationLinkListerner implements InitializingBean, DisposableBean {

	private final EventPublisher eventPublisher;

	/**
	 * Constructor
	 *
	 * @param eventPublisher the {@link EventPublisher} implementation
	 */
	protected AbstractJenkinsApplicationLinkListerner(EventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	/**
	 * Called when the plugin has been enabled.
	 *
	 * @throws Exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		eventPublisher.register(this);
	}

	/**
	 * Called when the plugin is being disabled or removed.
	 *
	 * @throws Exception
	 */
	@Override
	public void destroy() throws Exception {
		eventPublisher.unregister(this);
	}

	/**
	 * Check if the given {@link ApplicationLinkEvent} event object can be handled by this listener implementation
	 *
	 * @param event the {@link com.atlassian.applinks.api.event.ApplicationLinkEvent} event to check
	 * @return {@code true} if the ApplicationType of the event is the {@link JenkinsApplicationType}
	 */
	protected boolean supportsApplicationLinkType(ApplicationLinkEvent event) {
		if (event.getApplicationType() instanceof IdentifiableType) {
			IdentifiableType type = (IdentifiableType) event.getApplicationType();
			return type.getId().equals(JenkinsApplicationType.TYPE_ID) || type.getId().equals(HudsonApplicationType
					.TYPE_ID);
		}
		return false;
	}

}
