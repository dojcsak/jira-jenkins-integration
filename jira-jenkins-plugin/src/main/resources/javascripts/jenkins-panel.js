/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function initializeResultFilter() {
	var selection = getFilterSelection();
	AJS.$(".result-filter", "#dropdown-result-filters").each(function () {
		var filter = AJS.$(this).attr("data");
		if (selection[filter] || selection[filter] === undefined) {
			AJS.$(this).addClass("checked");
		} else {
			toggleBuildResults(filter, false);
		}
	});
	AJS.$(".result-filter", "#dropdown-result-filters").on({
		'aui-dropdown2-item-check': function () {
			toggleBuildResults(AJS.$(this).attr("data"), true);
		},
		'aui-dropdown2-item-uncheck': function () {
			toggleBuildResults(AJS.$(this).attr("data"), false);
		}
	});
}

function toggleBuildResults(resultType, show) {
	var results = AJS.$("." + resultType, ".jenkins-build-list");
	if (show) {
		results.show();
	} else {
		results.hide();
	}
	var selection = getFilterSelection();
	selection[resultType] = show;
	AJS.Cookie.save("JENKINS_BUILDS_PANEL_FILTERS", JSON.stringify(selection));
}

function getFilterSelection() {
	var cookie = AJS.Cookie.read("JENKINS_BUILDS_PANEL_FILTERS") || "{}";
	return AJS.$.parseJSON(cookie);
}
