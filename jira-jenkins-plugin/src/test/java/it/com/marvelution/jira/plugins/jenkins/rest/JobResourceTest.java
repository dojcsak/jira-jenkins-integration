/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.com.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Test case for the {@link com.marvelution.jira.plugins.jenkins.rest.JobResource} resource
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class JobResourceTest extends BaseRESTTest {

	public static final String SERVICE = "job";

	/**
	 * Constructor
	 *
	 * @param properties the JIRA {@link com.atlassian.jira.config.properties.ApplicationProperties}
	 */
	public JobResourceTest(ApplicationProperties properties) {
		super(properties);
	}

	/**
	 * Validate that the Job services are allowed for admin users
	 */
	@Test
	public void testGetAllAdminUserAllowed() {
		ClientResponse response = getAdminService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(200));
	}

	/**
	 * Validate that the Job services are not allowed for anonymous users
	 */
	@Test
	public void testGetAllAnonymousNotAllowed() {
		ClientResponse response = getService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that the Job services are not allowed for authenticated users
	 */
	@Test
	public void testGetAllNoAdminUserNotAllowed() {
		ClientResponse response = getUserService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that anonymous users can request a job build sync
	 */
	@Test
	public void testAnonymousAllowedToSyncJob() {
		ClientResponse response = getService(SERVICE, "1", "sync").post(ClientResponse.class);
		assertThat(response.getStatus(), not(401));
	}

	/**
	 * Validate that anonymous users can request the last build time of a job
	 */
	@Test
	public void testAnonymousAllowedToGetLastBuildTimestamp() {
		ClientResponse response = getService(SERVICE, "1", "lastbuildtime").post(ClientResponse.class);
		assertThat(response.getStatus(), not(401));
	}

	/**
	 * Validate that authenticated users can not delete build, only administrators can
	 */
	@Test
	public void testUserNotAllowedToDeleteBuilds() {
		ClientResponse response = getUserService(SERVICE, "1", "builds").delete(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that authenticated users can not change the linked state of a Job, only administrators can
	 */
	@Test
	public void testUserNotAllowedToChangeAutolinkedJobState() {
		ClientResponse response = getUserService(SERVICE, "1", "autolink").post(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that authenticated users can not request teh sync status, only administrators can
	 */
	@Test
	public void testUserNotAllowedToLookupSyncState() {
		ClientResponse response = getUserService(SERVICE, "1", "sync", "status").get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that authenticated users can not get a Job, only administrators can
	 */
	@Test
	public void testUserNotAllowedToGetAJob() {
		ClientResponse response = getUserService(SERVICE, "1").get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

}
