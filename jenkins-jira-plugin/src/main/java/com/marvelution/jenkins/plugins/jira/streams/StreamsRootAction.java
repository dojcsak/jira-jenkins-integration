/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.streams;

import hudson.Extension;
import hudson.model.UnprotectedRootAction;
import org.apache.commons.jelly.JellyContext;
import org.apache.commons.jelly.JellyException;
import org.apache.commons.jelly.XMLOutput;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletResponse;

/**
 * Root action for the Atlassian Streams API
 *
 * @author Mark Rekveld
 * @since 1.4.4
 */
@Extension
public class StreamsRootAction implements UnprotectedRootAction {

	public static final String URL_NAME = "streams";
	private final DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTime();
	private final DateTimeFormatter timeZoneFormatter = DateTimeFormat.forPattern("Z");

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	/**
	 * Request Handler for the feed itself. This will always return an empty feed since the feed is created from the JIRA cache
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws Exception in case of errors
	 */
	public void doFeed(StaplerRequest request, StaplerResponse response) throws Exception {
		JellyContext context = new JellyContext();
		context.setVariable("request", request);
		DateTime now = new DateTime();
		context.setVariable("timezone", timeZoneFormatter.withZone(now.getZone()).print(now));
		context.setVariable("date", dateTimeFormatter.print(now.toDateTime(DateTimeZone.UTC)));
		try {
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("application/atom+xml");
			context.runScript(getClass().getResource("feed.jelly"), XMLOutput.createXMLOutput(response.getWriter()));
		} catch (JellyException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace(response.getWriter());
		}
	}

	/**
	 * Request handler for the feed config action. It will always return an empty filters list.
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws Exception in case of errors
	 */
	public void doConfig(StaplerRequest request, StaplerResponse response) throws Exception {
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/vnd.atl.streams+json");
		response.getWriter().write("{\"filters\":[]}");
	}

}
