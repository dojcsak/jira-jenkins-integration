/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.store;

import com.google.common.base.Predicate;
import com.google.common.collect.*;
import com.marvelution.jenkins.plugins.jira.model.EntityLink;
import hudson.model.AbstractProject;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Entity Link Store
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class EntityLinkStore extends BaseStore {

	private final List<EntityLink> entityLinks = Lists.newArrayList();

	/**
	 * Constructor
	 *
	 * @param project the target {@link AbstractProject}
	 */
	private EntityLinkStore(AbstractProject project) {
		super(project.getRootDir());
	}

	/**
	 * Getter for the {@link EntityLinkStore} for a specific {@link AbstractProject}
	 *
	 * @param project the {@link AbstractProject} to get the {@link EntityLinkStore} for
	 * @return the {@link EntityLinkStore}
	 * @throws IOException in case of errors while loading an existing {@link EntityLinkStore}
	 */
	public static EntityLinkStore getStore(AbstractProject project) throws IOException {
		EntityLinkStore store = new EntityLinkStore(project);
		store.load();
		return store;
	}

	/**
	 * Get all the {@link EntityLink}s
	 *
	 * @return an {@link ImmutableList} of {@link EntityLink}s
	 */
	public ImmutableList<EntityLink> get() {
		return ImmutableList.copyOf(entityLinks);
	}

	/**
	 * Get all the {@link EntityLink}s in a {@link Map}
	 *
	 * @return the {@link EntityLink} {@link Map}
	 */
	public ImmutableMap<String, EntityLink> getAsMap() {
		Map<String, EntityLink> links = Maps.newHashMap();
		for (EntityLink link : entityLinks) {
			if (links.containsKey(link.getKey())) {
				if (!links.get(link.getKey()).isPrimary() && link.isPrimary()) {
					links.put(link.getKey(), link);
				}
			} else {
				links.put(link.getKey(), link);
			}
		}
		return ImmutableMap.copyOf(links);
	}

	/**
	 * Get if the store has any elements
	 *
	 * @return {@code true} if there are links, {@code false} otherwise
	 */
	public boolean hasLinks() {
		return !entityLinks.isEmpty();
	}

	/**
	 * Add a new {@link EntityLink} to this {@link EntityLinkStore}
	 *
	 * @param entityLink the {@link EntityLink} to add
	 * @throws IOException in case of write errors
	 */
	public synchronized void add(EntityLink entityLink) throws IOException {
		entityLinks.add(entityLink);
		save();
	}

	/**
	 * Remove an {@link EntityLink} from this {@link EntityLinkStore}
	 *
	 * @param typeId        the type Id
	 * @param key           the JIRA Project Key
	 * @param applicationId the application Id
	 * @throws IOException in case of write errors
	 */
	public synchronized void remove(final String typeId, final String key, final String applicationId) throws IOException {
		try {
			EntityLink link = Iterables.find(entityLinks, new Predicate<EntityLink>() {
				@Override
				public boolean apply(EntityLink entityLink) {
					return entityLink.getTypeId().equals(typeId) && entityLink.getKey().equals(key) && entityLink
							.getApplicationId().equals(applicationId);
				}
			});
			if (link != null) {
				entityLinks.remove(link);
				save();
			}
		} catch (NoSuchElementException e) {
			// Ignore
		}
	}

}
