/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.utils;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Utils for URL/URI related objects
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class UriUtils {

	/**
	 * The request attribute key that the request dispatcher uses to store the
	 * original URL for a forwarded request.
	 */
	public static final String FORWARD_REQUEST_URI = "javax.servlet.forward.request_uri";

	/**
	 * Get the logical URI for the given {@code request}
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest}
	 * @return the forwarded URI or {@code null}
	 */
	public static String getLogicalUri(HttpServletRequest request) {
		String uriPathBeforeForwarding = (String) request.getAttribute(FORWARD_REQUEST_URI);
		if (uriPathBeforeForwarding == null) {
			return null;
		}
		URI newUri = URI.create(request.getRequestURL().toString());
		try {
			return new URI(newUri.getScheme(), newUri.getAuthority(), uriPathBeforeForwarding, newUri.getQuery(),
					newUri.getFragment()).toString();
		} catch (URISyntaxException e) {
			return null;
		}
	}

}
