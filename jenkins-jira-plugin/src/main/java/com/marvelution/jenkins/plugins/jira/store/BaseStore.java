/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.store;

import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import com.thoughtworks.xstream.XStream;
import hudson.XmlFile;
import hudson.model.Hudson;

import java.io.File;
import java.io.IOException;

/**
 * Base Store
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class BaseStore {

	private final transient File rootDir;
	private final transient String filename;
	private final transient XStream xStream;

	/**
	 * Constructor
	 *
	 * @param rootDir the root directory to load the default file from
	 * @see #BaseStore(java.io.File, String) BaseStore(rootDir, JIRAPlugin.getPluginShortName() + ".xml")
	 */
	protected BaseStore(File rootDir) {
		this(rootDir, JIRAPlugin.getPluginShortName() + ".xml");
	}

	/**
	 * Constructor
	 *
	 * @param rootDir  the root directory to load the given {@code filename} file from
	 * @param filename the name of the store file to load
	 * @see #BaseStore(java.io.File, String, com.thoughtworks.xstream.XStream) BaseStore(rootDir, filename, Hudson.XSTREAM)
	 */
	protected BaseStore(File rootDir, String filename) {
		this(rootDir, filename, Hudson.XSTREAM);
	}

	/**
	 * Constructor
	 *
	 * @param rootDir  the root directory to load the given {@code filename} file from
	 * @param filename the name of the store file to load
	 * @param xStream  the {@link com.thoughtworks.xstream.XStream} instance to use
	 */
	protected BaseStore(File rootDir, String filename, XStream xStream) {
		this.rootDir = rootDir;
		this.filename = filename;
		this.xStream = xStream;
	}

	/**
	 * Load the Store from the configuration {@link File}
	 *
	 * @throws IOException in case of load errors
	 */
	protected synchronized void load() throws IOException {
		XmlFile xml = getConfigXml();
		if (xml.exists()) {
			xml.unmarshal(this);
		}
	}

	/**
	 * Save the Store to the configuration {@link File}
	 *
	 * @throws IOException in case of save errors
	 */
	protected synchronized void save() throws IOException {
		XmlFile config = getConfigXml();
		config.write(this);
	}

	/**
	 * Getter for the {@link XmlFile}
	 *
	 * @return the {@link XmlFile}
	 */
	private XmlFile getConfigXml() {
		return new XmlFile(xStream, new File(rootDir, filename));
	}

}
