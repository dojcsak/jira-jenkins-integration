/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.filter;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.marvelution.jenkins.plugins.jira.applinks.BaseActionTest;
import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.xml.sax.SAXException;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for Application Link requests
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ApplinksServletFilterTest extends BaseActionTest {

	@Test
	public void testAnonymousAccessToImages() throws IOException, SAXException {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.goTo(PluginUtils.getPluginArifactId() + "/static/images/icon16_jenkins.png", "image/png");
	}

	@Test
	public void testAuthConfCall() throws IOException, SAXException {
		JenkinsRule.WebClient webClient = getWebClient();
		HtmlPage page = webClient.goTo("plugins/servlet/applinks/auth/conf/basic.html");
		assertThat(page.asText(), is("Application Link Auth Configuration\nNot yet supported.\n"));
	}

}
