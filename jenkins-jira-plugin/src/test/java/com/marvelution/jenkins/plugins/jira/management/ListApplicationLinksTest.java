/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.management;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ListApplicationLinksTest {

	@Mock
	private StaplerRequest request;
	@Mock
	private StaplerResponse response;
	@Mock
	private RequestDispatcher dispatcher;
	private ListApplicationLinks links;
	private Map<String, String> parameters;

	@Before
	public void setup() throws IOException {
		links = new ListApplicationLinks();
	}

	private void configureMocks() throws IOException {
		when(request.getParameterMap()).thenReturn(parameters);
		when(request.getParameter(anyString())).thenAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return URLDecoder.decode(parameters.get(invocation.getArguments()[0]), "UTF-8");
			}
		});
		when(request.getView(eq(links), anyString())).thenReturn(dispatcher);
	}

	@Test
	public void testDoIndexConnectionException() throws Exception {
		parameters = ImmutableMap.of("applinkCreationStatusLog",
				"%22%5B%7B%5C%22status%5C%22%3A%5C%22inprogress" +
						".creation%5C%22%7D%2C%7B%5C%22status%5C%22%3A%5C%22completed.creation" +
						".local%5C%22%7D%2C%7B%5C%22status%5C%22%3A%5C%22inprogress.creation.reciprocal%5C%22%7D%5D%22",
				"applinkOriginalCreatedId", "%22df0c5cf1-0a1f-37a7-9977-c139567a593e%22",
				"applinkStartingUrl", "%22http%3A%2F%2Flocalhost%3A2990%2Fjira%22",
				"applinkStartingType", "%22jira%22", "sharedUserbase", "false"
		);
		configureMocks();
		links.doIndex(request, response);
		verify(request).getView(links, ListApplicationLinks.INVALID_RECIPROCAL_VIEW);
		verify(dispatcher).forward(request, response);
	}

	@Test
	public void testDoIndexReciprocalView() throws Exception {
		parameters = ImmutableMap.of("applinkCreationStatusLog",
				"%22%5B%7B%5C%22status%5C%22%3A%5C%22inprogress" +
						".creation%5C%22%7D%2C%7B%5C%22status%5C%22%3A%5C%22completed.creation" +
						".local%5C%22%7D%2C%7B%5C%22status%5C%22%3A%5C%22inprogress.creation.reciprocal%5C%22%7D%5D%22",
				"applinkOriginalCreatedId", "%2227274ce6-d606-3a64-b0bb-df07fa4b4d00%22",
				"applinkStartingUrl", "%22https%3A%2F%2Fmarvelution.atlassian.net%22",
					"applinkStartingType", "%22jira%22", "sharedUserbase", "false"
		);
		configureMocks();
		links.doIndex(request, response);
		verify(request).getView(links, ListApplicationLinks.RECIPROCAL_VIEW);
		verify(dispatcher).forward(request, response);
	}

	@Test
	public void testDoIndex() throws Exception {
		parameters = Maps.newHashMap();
		configureMocks();
		links.doIndex(request, response);
		verify(request).getView(links, ListApplicationLinks.LIST_View);
		verify(dispatcher).forward(request, response);
	}

}
